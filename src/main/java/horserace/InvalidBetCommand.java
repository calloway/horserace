package horserace;

public class InvalidBetCommand extends ATMCommand<String> {
  public InvalidBetCommand(String betAmount) {
    super(ATMCommandType.INVALIDBET, betAmount);
  }

  public String getBetAmount() {
    return getValue1();
  }
  @Override public String executeOn(ATMMachine machine) {
    // no op
    return "Invalid Bet: " + getBetAmount();
  }
}
