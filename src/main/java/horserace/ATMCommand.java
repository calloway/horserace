package horserace;


public abstract class ATMCommand<T> {
  private ATMCommandType type;

  public T getValue1() {
    return value1;
  }

  private T value1;

  public T getValue2() {
    return value2;
  }

  private T value2;

  protected ATMCommand() {}
  public ATMCommand(ATMCommandType type) {
    this.type = type;
  }
  public ATMCommand(ATMCommandType type, T value1) {
    this.type = type;
    this.value1 = value1;
  }
  public ATMCommand(ATMCommandType type, T value1, T value2) {
    this.type = type;
    this.value1 = value1;
    this.value2 = value2;
  }
  public boolean isQuit() {
    return type.equals(ATMCommandType.QUIT);
  }
  @Override public String toString() {
    return type.name() +" " + ( value1 != null? value1 : "")  + " " + ( value2 != null? value2 : "");
  }

  public abstract String executeOn(ATMMachine machine);


}
