package horserace;

public class Horse {
  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getOdds() {
    return odds;
  }

  private int id;
  private String name;
  private int odds;

  private Horse() {}

  public Horse(int id, String name, int odds) {
    this.id = id;
    this.name = name;
    this.odds = odds;
  }
}
