package horserace;

public class SetWinningHorseCommand extends ATMCommand<Integer> {
  public SetWinningHorseCommand(int horseId) {
    super(ATMCommandType.SETWINNINGHORSE, horseId);
  }
  public SetWinningHorseCommand(String horseId) {
    super(ATMCommandType.SETWINNINGHORSE, Integer.parseInt(horseId));
  }
  public int getHorseID() { return getValue1(); }

  @Override public String executeOn(ATMMachine machine) {
    // no op
    machine.setWinningHorseID(getHorseID());
    return "";
  }
}
