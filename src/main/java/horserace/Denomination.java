package horserace;

public enum Denomination {
  WASHINGINGTON(1),
  JEFFERSON(2),
  LINCOLN(5),
  HAMILTON(10),
  JACKSON(20),
  GRANT(50),
  FRANKLIN(100);

  Denomination(int dollarValue) {
    this.dollarValue = dollarValue;
  }

  public int getDollarValue() {
    return dollarValue;
  }

  private int dollarValue;
}
