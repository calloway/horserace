package horserace;


public enum ATMCommandType {
  RESTOCK,
  QUIT,
  SETWINNINGHORSE,
  BETHORSE,
  INVALIDBET,
  INVALIDHORSE,
  INVALID;
}
