package horserace;

import java.util.*;

public class CashBox {
  public Map<Denomination, Integer> getBills() {
    return bills;
  }

  private Map<Denomination, Integer> bills = new TreeMap<Denomination, Integer>((o1, o2) -> o2.getDollarValue() - o1.getDollarValue());

  public static String toString(Map<Denomination, Integer> cash) {
    StringBuffer ret = new StringBuffer();
    cash.entrySet().forEach( i -> ret.append("$" + i.getKey().getDollarValue() +"," + i.getValue() +"\n"));
    return ret.toString();
  }

  public CashBox() {
    restock();
  }
  public boolean hasEnoughCash(int amount) {
    return totalCash() >= amount;
  }

  public void withSingleBill(Denomination bill) {
    bills.put(bill, bills.get(bill).intValue() - 1);
  }

  // only in java 8
  public int totalCash() {
    return bills.entrySet().stream().mapToInt(i -> i.getKey().getDollarValue() * i.getValue()).sum();
  }

  public synchronized  void restock() {
    bills.clear();
    bills.put(Denomination.WASHINGINGTON, 10);
    bills.put(Denomination.GRANT, 10);
    bills.put(Denomination.HAMILTON, 10);
    bills.put(Denomination.JACKSON, 10);
    bills.put(Denomination.LINCOLN, 10);
    bills.put(Denomination.FRANKLIN, 10);
  }
}
