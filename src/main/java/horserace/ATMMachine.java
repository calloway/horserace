package horserace;

import javax.swing.text.html.Option;
import java.util.*;

public class ATMMachine {
  public CashBox getCashBox() {
    return cashBox;
  }

  public void setCashBox(CashBox cashBox) {
    this.cashBox = cashBox;
  }

  private CashBox cashBox = new CashBox();
  private Map<Integer, Horse> horses = new HashMap();

  public int getWinningHorseID() {
    return winningHorseID;
  }

  public Horse getHorseById(int id) {
    return horses.get(id);
  }

  public void setWinningHorseID(int winningHorseID) {
    this.winningHorseID = winningHorseID;
  }

  public synchronized Map<Denomination, Integer> withDrawCash(int amount) {
    return withDrawCash(amount, new TreeMap<Denomination, Integer>((o1, o2) -> o2.getDollarValue() - o1.getDollarValue()));
  }

  public String getHorsesString() {
    StringBuffer ret = new StringBuffer("Horses:\n");
    horses.values().forEach( i -> ret.append(i.getId() + ", " + i.getName() +", " + i.getOdds() +", " + (i.getId() == getWinningHorseID()? "won\n": "lost" + "\n")));
    return ret.toString();
  }

  private synchronized Map<Denomination, Integer> withDrawCash(int amount, Map<Denomination, Integer> cash) {
    if (amount == 0) return cash;
    Optional<Map.Entry<Denomination,Integer>> billOption = cashBox.getBills ().entrySet().stream().filter(i -> i.getValue() > 0
      && i.getKey().getDollarValue() <= amount).findFirst();
    if (billOption.isPresent()) {
      Map.Entry<Denomination,Integer> entry = billOption.get();
      Denomination denomination = entry.getKey();
      int existing = cash.containsKey(denomination) ? cash.get(denomination) : 0;
      cash.put(denomination, existing + 1);
      cashBox.withSingleBill(denomination);
      withDrawCash(amount - denomination.getDollarValue(), cash);
    } else {
      throw new RuntimeException("Not enough cash in cashBox to withdraw " + amount);
    }
    return cash;
  }

  public synchronized String payoutWinnings(int betAmount) {
    Horse winningHorse = horses.get(winningHorseID);
    int winningAmount = winningHorse.getOdds() * betAmount;
    if (cashBox.hasEnoughCash(winningAmount)) {
      Map<Denomination, Integer> cash = withDrawCash(winningAmount);
      return "Payout: " + winningHorse.getName() + " $"+ winningAmount +"\nDispensing:\n" + CashBox.toString(cash);

    }
    return "Not enough cash";
  }
  private int winningHorseID = 1;

  public ATMMachine() {
    setHorses();
  }
  private void setHorses() {
    horses.put(1, new Horse(1, "That Darn Gray Cat", 5));
    horses.put(2, new Horse(2, "Fort Utopia", 10));
    horses.put(3, new Horse(3, "Count Sheep", 9));
    horses.put(4, new Horse(4, "Ms Traitour", 4));
    horses.put(5, new Horse(5, "Real Princess", 3));
    horses.put(6, new Horse(6, "Pa Kettle", 5));
    horses.put(7, new Horse(7, "Gin Stinger", 6));
  }
}
