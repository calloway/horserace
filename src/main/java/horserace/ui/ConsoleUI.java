package horserace.ui;


import horserace.ATMCommand;
import horserace.ATMMachine;
import horserace.CashBox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleUI {

  /*
   'R' or 'r' - restocks the cash inventory
   'Q' or 'q' - quits the application
   'W' or 'w' [1-7] - sets the winning horse number
   [1-7] <amount> - specifies the horse wagered on and the amount of the bet
  */
  public static void main(String[] args) {
    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
    try {
      ATMCommand command = null;
      ATMCommandParser parser = new ATMCommandParser();
      ATMMachine machine = new ATMMachine();
      do {
        command = parser.ToATMCommand(bufferRead.readLine());
        String result = command.executeOn(machine);
        System.out.println(result);
        System.out.println(CashBox.toString(machine.getCashBox().getBills()));
        System.out.println(machine.getHorsesString());
      }
      while (!command.isQuit());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
