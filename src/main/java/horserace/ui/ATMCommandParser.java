package horserace.ui;

import horserace.*;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ATMCommandParser {
  private static Map<ATMCommandType, String> regexInfo = new HashMap<>();

  static {
    regexInfo.put(ATMCommandType.RESTOCK,"^[Rr] *$");
    regexInfo.put(ATMCommandType.QUIT,"^[Qq] *$");
    regexInfo.put(ATMCommandType.SETWINNINGHORSE,"^[Ww] *(\\d+) *$");
    regexInfo.put(ATMCommandType.BETHORSE,"^([1-7]) *(\\d+) *$");
    regexInfo.put(ATMCommandType.INVALIDHORSE,"^([^1-7]) *(\\d+) *$");
    regexInfo.put(ATMCommandType.INVALIDBET,"^\\d+ *(\\d*\\.\\d+)");
  }

  private Pattern winningHorseRegex = Pattern.compile(regexInfo.get(ATMCommandType.SETWINNINGHORSE));
  private Pattern betHorseRegex = Pattern.compile(regexInfo.get(ATMCommandType.BETHORSE));
  private Pattern invalidBetRegex = Pattern.compile(regexInfo.get(ATMCommandType.INVALIDBET));
  private Pattern invalidHorseRegex = Pattern.compile(regexInfo.get(ATMCommandType.INVALIDHORSE));

  public ATMCommand ToATMCommand(String input) {
    if (input.matches(regexInfo.get(ATMCommandType.RESTOCK))) return new RestockCommand();
    if (input.matches(regexInfo.get(ATMCommandType.QUIT))) return new QuitCommand();
    Matcher winningHorseMatcher = winningHorseRegex.matcher(input);
    if (winningHorseMatcher.find()) {
      return new SetWinningHorseCommand(winningHorseMatcher.group(1));
    }
    Matcher betHorseMatcher = betHorseRegex.matcher(input);
    if (betHorseMatcher.find()) {
      return new BetHorseCommand(betHorseMatcher.group(1), betHorseMatcher.group(2));
    }
    Matcher invalidBetMatcher = invalidBetRegex.matcher(input);
    if (invalidBetMatcher.find()) {
      return new InvalidBetCommand(invalidBetMatcher.group(1));
    }
    Matcher invalidHorseMatcher = invalidHorseRegex.matcher(input);
    if (invalidHorseMatcher.find()) {
      return new InvalidHorseCommand(invalidHorseMatcher.group(1));
    }
    return new InvalidCommand(input);
  }
}
