package horserace;

public class BetHorseCommand extends ATMCommand<Integer> {
  public BetHorseCommand(int horseId, int betAmount) {
    super(ATMCommandType.BETHORSE, horseId, betAmount);
  }
  public BetHorseCommand(String horseId, String betAmount) {
    super(ATMCommandType.BETHORSE, Integer.parseInt(horseId), Integer.parseInt(betAmount));
  }

  public int getBetAmount() {
    return getValue2();
  }

  @Override public String executeOn(ATMMachine machine) {
    int horseId = this.getValue1();
    if (horseId == machine.getWinningHorseID()) {
      String ret = machine.payoutWinnings(getBetAmount());

      return ret;

    }
    return "No Payout:" + machine.getHorseById(horseId).getName();
  }
}
