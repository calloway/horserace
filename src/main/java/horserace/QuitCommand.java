package horserace;

public class QuitCommand extends ATMCommand {
  public QuitCommand() {
    super(ATMCommandType.QUIT);
  }
  
  @Override public String executeOn(ATMMachine machine) {
    // no op
    return "";
  }
}
