package horserace;

public class InvalidHorseCommand extends ATMCommand<Integer> {
  public InvalidHorseCommand(int horseID) {
    super(ATMCommandType.INVALIDHORSE, horseID);
  }
  public InvalidHorseCommand(String horseID) {
    this(Integer.parseInt(horseID));
  }
  public Integer getHorseNumber() {
    return getValue1();
  }
  @Override public String executeOn(ATMMachine machine) {
    // no op
    return "Invalid Horse Number:" + getHorseNumber();
  }
}
