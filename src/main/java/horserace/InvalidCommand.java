package horserace;

public class InvalidCommand extends ATMCommand {
  public InvalidCommand(String input) {
    super(ATMCommandType.INVALID, input);
  }
  
  @Override public String executeOn(ATMMachine machine) {
    // no op
    return "Invalid Command: " + getValue1();
  }
}
