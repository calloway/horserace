package horserace;

public class RestockCommand extends ATMCommand {
  public RestockCommand() {
    super(ATMCommandType.RESTOCK);
  }

  @Override public String executeOn(ATMMachine machine) {
    machine.setCashBox(new CashBox());
    return "";
  }
}
