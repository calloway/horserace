package horserace.ui;

import horserace.ATMCommand;
import horserace.InvalidHorseCommand;
import horserace.RestockCommand;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* ATMCommandParser Tester. 
* 
* @author <Authors name> 
* @since <pre>Feb 13, 2014</pre> 
* @version 1.0 
*/ 
public class ATMCommandParserTest { 

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: ToATMCommand(String input) 
* 
*/ 
@Test
public void testToATMCommand1() throws Exception {
//TODO: Test goes here...
  ATMCommandParser parser = new ATMCommandParser();

  ATMCommand command = parser.ToATMCommand("r");
  Assert.assertThat(command, instanceOf(RestockCommand.class));
}

  @Test
  public void testToATMCommand2() throws Exception {
//TODO: Test goes here...
    ATMCommandParser parser = new ATMCommandParser();

    ATMCommand command = parser.ToATMCommand("9 4");
    Assert.assertThat(command, instanceOf(InvalidHorseCommand.class));
    Assert.assertEquals(command.getValue1(), 9);
  }
} 
