package horserace;

import horserace.CashBox;
import horserace.Denomination;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

/** 
* CashBox Tester. 
* 
* @author <Authors name> 
* @since <pre>Feb 12, 2014</pre> 
* @version 1.0 
*/ 
public class CashBoxTest { 

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getBills() 
* 
*/ 
@Test
public void testGetBills() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: toString(Map<Denomination, Integer> cash) 
* 
*/ 
@Test
public void testToString() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: hasEnoughCash(int amount) 
* 
*/ 
@Test
public void testHasEnoughCash() throws Exception { 
//TODO: Test goes here...
  CashBox box = new CashBox();
  Assert.assertFalse(box.hasEnoughCash(99999));
  Assert.assertTrue(box.hasEnoughCash(275));
} 

/** 
* 
* Method: totalCash() 
* 
*/ 
@Test
public void testTotalCash() throws Exception { 
//TODO: Test goes here...
  CashBox box = new CashBox();
  int initialCash = box.totalCash();
  box.withSingleBill(Denomination.JACKSON);
  Assert.assertEquals(initialCash - Denomination.JACKSON.getDollarValue(), box.totalCash());
} 

/** 
* 
* Method: restock() 
* 
*/ 
@Test
public void testRestock() throws Exception { 
//TODO: Test goes here...
  CashBox box = new CashBox();
  int initialCash = box.totalCash();
  box.withSingleBill(Denomination.JACKSON);

  box.restock();
  int restockedCash = box.totalCash();
  Assert.assertEquals(initialCash, restockedCash);
} 


} 
